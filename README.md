Play Framework Tutorial
=======================

## How to start coding ?

* Git clone ``` git clone repoUrl ```

* Go to directory  ``` cd playTutorial ```

* Adapt project for IDEA  ``` play idealize ```

* Open project Project->Open. Select *.iml
