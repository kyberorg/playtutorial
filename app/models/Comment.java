package models;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.Model;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import java.util.Date;

/**
 * Comment model
 *
 * @author Alexander Muravya {@literal <muravya@corp.sputnik.ru>}
 * @since 1.0
 */
@Entity
public class Comment extends Model {

    @Required
    public String author;

    @Required
    public Date postedAt;

    @Lob
    @Required
    @MaxSize(10000)
    public String content;

    @ManyToOne
    @Required
    public Post post;

    public Comment(Post post, String author, String content) {
        this.post = post;
        this.author = author;
        this.content = content;
        this.postedAt = new Date();
    }

    @Override
    public String toString() {
        String str = String.format("Comment for '%s' by '%s' at '%s' ", post.title, author, postedAt);
        return str;
    }
}
