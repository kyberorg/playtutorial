package controllers;

import models.User;
import play.mvc.With;

/**
 * Users Adminka
 *
 * @author Alexander Muravya (alexander.muravya@kuehne-nagel.com)
 * @since 1.0
 */
@Check("admin")
@With(Secure.class)
public class Users extends CRUD {
    
}
