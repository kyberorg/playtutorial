package controllers;

import play.mvc.With;

/**
 * Post CRUD
 *
 * @author Alexander Muravya (alexander.muravya@kuehne-nagel.com)
 * @since 1.0
 */
@Check("admin")
@With(Secure.class)
public class Posts extends CRUD {

}
