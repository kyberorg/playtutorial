import models.User;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import play.test.Fixtures;

/**
 * StartUp actions
 *
 * @author Alexander Muravya {@literal <muravya@corp.sputnik.ru>}
 * @since 1.0
 */
@OnApplicationStart
public class Bootstrap extends Job {
    @Override
    public void doJob() throws Exception {
        // Check if the database is empty
        if(User.count() == 0) {
            Fixtures.loadModels("init-data.yml");
        }
    }
}
